"""
    Author : jesuisnuageux
    Date : 23/03/2015
    Function : Handle wifi auth in my school
"""
from urllib import request, parse, error
from os import path


DEBUG=True

class App:
    def __init__(self, conf_file='~/.connectIUT'):
        self.refUrl = "http://www.google.fr/"
        self.fingerprint = "paris5"
        self.connectUrl = ""
        self.conf_file = conf_file
        self.credential = {"auth_user": "", "auth_pass": "","accept":"Continue"}
        if path.exists(conf_file):
            with open(conf_file, 'r') as src:
                for line in src:
                    if 'username : ' in line:
                        self.credential['auth_user'] = line.split(' : ')[1].strip()
                    if 'password : ' in line:
                        self.credential['auth_pass'] = line.split(' : ')[1].strip()
        else:
            print("/!\\ No '{}' found in current directory /!\\".format(conf_file))
            exit()
        if self.credential["auth_user"] == "" or self.credential["auth_pass"] == "":
            print("/!\\ Unable to get both username and password /!\\")
            exit()
        return

    def getConnectUrl(self):
        self.connectUrl = request.urlopen(self.refUrl).geturl()
        if self.connectUrl == self.refUrl:
            print("You maybe allready be connected")
        return

    def connectUser(self):
        data = parse.urlencode(self.credential).encode("utf-8")
        req = request.Request(self.connectUrl, data)
        try:
            ans = request.urlopen(req)
        except error.HTTPError as e:
            print("Error : {}".format(e))
            return None
        if not ans.geturl == self.refUrl:
            print("'{}' and '{}' are different...".format(ans.geturl(), self.refUrl))
        return ans.geturl == self.refUrl
    def testConnect(self):
        return self.refUrl==request.urlopen(self.refUrl).geturl()
    def run(self):
        self.getConnectUrl()
        if DEBUG:
            print("Reference URL is : '{}'".format(self.refUrl))
            print("Requesting '{}'".format(self.connectUrl))
            print("Used username : '{}'".format(self.credential['auth_user']))
        result = self.connectUser()
        if DEBUG:
            print("Connected : {}".format(self.testConnect()))